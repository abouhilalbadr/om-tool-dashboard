module.exports = {
    devServer: {
        proxy: {
            "/api/*": {
                target: "http://localhost:3010",
                pathRewrite: {'^/api': ''},
                secure: false
            }
        }
    }
}
