import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    superadmin: localStorage.getItem('superadmin') || '',
    name: localStorage.getItem('name') || '',
    id: localStorage.getItem('id') || '',
    email: localStorage.getItem('email') || '',
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, args){
      state.status = 'success'
      state.token = args.token
      state.name = args.user.username
      state.id = args.user.id
      state.superadmin = args.user.superadmin
      state.email = args.user.email
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
      state.name = ''
      state.id = ''
      state.superadmin = ''
      state.email = ''
    },
  },
  actions: {
    login({commit}, user){
      return new Promise((resolve, reject) => {
          commit('auth_request')
          axios({ method: 'POST', url: '/login', data: user})
          .then(resp => {
              const args = {token: resp.data.token, user: resp.data.user}
              localStorage.setItem('token', args.token)
              localStorage.setItem('name', args.user.username)
              localStorage.setItem('id', args.user.id)
              localStorage.setItem('superadmin', args.user.superadmin)
              localStorage.setItem('email', args.user.email)
              axios.defaults.headers.common['Authorization'] = args.token
              commit('auth_success', args)
              resolve(resp)
          })
          .catch(err => {
              commit('auth_error')
              localStorage.removeItem('token')
              localStorage.removeItem('name')
              localStorage.removeItem('id')
              localStorage.removeItem('superadmin')
              localStorage.removeItem('email')
              reject(err)
          })
      })
    },
    logout({commit}){
      return new Promise((resolve, reject) => {
          commit('logout')
          localStorage.removeItem('token')
          localStorage.removeItem('name')
          localStorage.removeItem('id')
          localStorage.removeItem('superadmin')
          localStorage.removeItem('email')
          localStorage.removeItem('sidenav')
          delete axios.defaults.headers.common['Authorization']
          resolve()
      })
    },
    updateImg({commit}, user) {
      return new Promise((resolve, reject) => {
          commit('auth_request')
          axios
          .post(`/user/${user.userid}/picture`, user.picture, {
              headers: {
                  'Authorization': user.token,
                  'x-user-id': user.userid,
                  'Content-Type': 'multipart/form-data'
              }
          })
          .then(resp => {
              resolve(resp)
          })
          .catch(err => {
              reject(err)
          })
      })
    },
    createEvent({commit}, data) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios
        .post(`/event`, data.event, {
            headers: {'Authorization': data.token, 'x-user-id': data.userid}
        })
        .then(resp => {
            resolve(resp)
        })
        .catch(err => {
            reject(err)
        })
    })
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    isSuperAdmin: state => state.superadmin,
    authStatus: state => state.status,
  },
  modules: {
  }
})
