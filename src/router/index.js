import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Login from '../views/Login.vue'

Vue.use(Router)

let router =  new Router({
  routes: [
    { 
      path: '/',
      name: 'Login',
      component: Login,
      meta: { title: "Login", hidesides: true }
    },
    { 
      path: '/dashboard',
      name: 'Dashboard',
      component: () => import('../views/Dashboard.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Dashboard" }
    },
    { 
      path: '/registration',
      name: 'Registration',
      component: () => import('../views/Registration.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Registration", hidesides: true }
    },
    { 
      path: '/events',
      name: 'Events',
      component: () => import('../views/Events.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Events" }
    },
    { 
      path: '/events/:id/:title',
      name: 'EventSettings',
      component: () => import('../views/EventSettings.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Event Settings" }
    },
    { 
      path: '/events/:id/:title/:session',
      name: 'EventSession',
      component: () => import('../views/EventSession.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Event Session" }
    },
    {
      path: '/users',
      name: 'Users',
      component: () => import('../views/Users.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Users" }
    },
    {
      path: '/admins',
      name: 'Admins',
      component: () => import('../views/Admins.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Admins" }
    },
    {
      path: '/visualizations',
      name: 'Visualizations',
      component: () => import('../views/Visualizations.vue'),
      meta: { requiresAuth: true, isSuperAdmin: true, title: "Visualizations" }
    },
    {
      path: '/404',
      name: 'NotFound',
      component: () => import('../views/NotFound.vue'),
      meta: { title: "404 Not Found", hidesides: true }
    },  
    {
      path: '*',
      redirect: '/404'
    },

  ],
  // mode: 'history'
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isSuperAdmin) {
          next()
          return
      }
      else if (store.getters.isLoggedIn) {
          next()
          return
      }        
      next('/') 
  }
  // else if (to.matched.some(record => record.meta.hidesides)) {
  //   store.state.showSides = false
  //   next()
  // }
  else
      next()
})

export default router
