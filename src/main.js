import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store'

import env from './env'

import VueParticles from 'vue-particles'
import VueSweetalert2 from 'vue-sweetalert2'

import '@/assets/tailwind.css'
import 'vue-select/dist/vue-select.css'
import 'sweetalert2/dist/sweetalert2.min.css'


Vue.use(VueParticles)
Vue.use(VueSweetalert2)

Vue.config.productionTip = false

axios.defaults.baseURL = env.baseURL

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
